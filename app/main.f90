!> \file    main.f90
!> \copydoc main

!> \brief   Example application
!> \details This program performs something very simple. But you should describe it.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    May 2021
!> \copyright Copyright 2005-\today, the CHS Developers, Sabine Attinger: All rights reserved.
!! This code is released under the LGPLv3+ license \license_note
program main

  use mo_kind,              only: dp
  use mo_your_module,       only: first_two_moments
  use mo_your_program_info, only: version, version_date

  implicit none

  real(dp), dimension(10) :: dat
  real(dp), dimension(2) :: output

  dat = [1., 2., 3., 4., 5., 6., 7., 8., 9., 10.]

  output = first_two_moments(dat)
  print*, "first two moments of", dat, "are: ", output

  print*, "Program version: ", version
  print*, "Program date: ", version_date

end program main
