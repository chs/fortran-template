!> \file    mo_your_module.f90
!> \copydoc mo_your_module

!> \brief   Your module
!> \details This module performs something very simple. But you should describe it.
!> \version 0.1
!> \authors Sebastian Mueller
!> \date    May 2021
!> \copyright Copyright 2005-\today, the CHS Developers, Sabine Attinger: All rights reserved.
!! This code is released under the LGPLv3+ license \license_note
module mo_your_module

  use mo_kind,   only: i4, dp

  implicit none
  private
  public :: first_two_moments ! first two statistical moments of an array

contains

  !> \brief   Calculate first two statistical moments of an array.
  !> \return  array with first two moments
  function first_two_moments(dat) result(output)

    use mo_moment, only: central_moment

    implicit none

    !> array with values to analyze
    real(dp), dimension(:), intent(in) :: dat

    ! output
    real(dp), dimension(2) :: output
    ! looping variable
    integer(i4) :: i

    do i = 1, 2
      output(i) = central_moment(dat, i)
    end do

  end function first_two_moments

end module mo_your_module
