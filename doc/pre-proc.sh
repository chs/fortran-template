#!/bin/bash
version="'"$(cat version.txt)"'"
version_date="'"$(git log -1 --format=%cd --date=format:%Y-%m-%d)"'"
if [[ -n $FC && -n $(command -v $FC) ]]; then
  if [[ $FC == *nagfor ]]; then
    $FC -fpp -F -DPROJVERSION="${version}" -DPROJDATE="${version_date}" -o tempfile-doxygen.fpp $1 &>/dev/null
    cat tempfile-doxygen.fpp
    rm -f tempfile-doxygen.fpp
  else
    $FC -cpp -E -DPROJVERSION="${version}" -DPROJDATE="${version_date}" $1
  fi
else
  if [[ -n $(command -v gfortran) ]]; then
    gfortran -cpp -E -DPROJVERSION="${version}" -DPROJDATE="${version_date}" $1
  fi
fi
